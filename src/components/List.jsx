import React, { useState } from "react";
import ListItem from "./ListItem";

const List = () => {
  const [list, setList] = useState([]);
  const [count, setCount] = useState(0);

  const addItem = () => {
    const newItem = `Artículo ${count + 1}`;
    console.log(newItem);
    setList([...list, newItem]);
    setCount(count + 1);
  };

  return (
    <React.Fragment>
      <button onClick={addItem}>Añadir</button>
      <div>
        {list.map((index, item) => (
          <ListItem key={index} id={item + 1} />
        ))}
      </div>
      <button onClick={() => setList([])}>Vaciar</button>
    </React.Fragment>
  );
};

export default List;
