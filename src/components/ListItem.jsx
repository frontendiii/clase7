import React from "react";

const ListItem = ({ id }) => {
  return (
    <React.Fragment>
        <p>{`${id} añadido a la lista`}</p>
    </React.Fragment>
  )
}

export default ListItem;